package pl.polsl.pscf.heatingsystem

import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.microsoft.azure.sdk.iot.service.devicetwin.DeviceMethod;
import com.microsoft.azure.sdk.iot.service.devicetwin.MethodResult;
import java.io.IOException

// Link to azure device android (Java xD) example
// https://github.com/Azure-Samples/azure-iot-samples-java/blob/master/iot-hub/Samples/device/AndroidSample/app/src/main/java/com/microsoft/azure/iot/sdk/samples/androidsample/MainActivity.java

class MainActivity : AppCompatActivity() {

    private var setpoint = 20f
    private var setAutomatic = false;
    private var setManual = false;
    private var isAutomatic = false
    private var heaterOn = false
    private var turnHeaterOn = false
    private var turnHeaterOff = false
    private var deviceOnline = false

    private lateinit var onlineTextView: TextView
    private lateinit var temperatureTextView: TextView
    private lateinit var heaterTextView: TextView
    private lateinit var modeTextView: TextView
    private lateinit var setpointTextView: TextView

    var methodClient: DeviceMethod? = null

    private val connString: String = ""
    private val deviceId: String = ""
    private var lastException: String = ""
    private val sendMessagesInterval = 5000
    var result: MethodResult? = null

    private val handler: Handler = Handler()

    private val responseTimeout: Long = java.util.concurrent.TimeUnit.SECONDS.toSeconds(200)
    private val connectTimeout: Long = java.util.concurrent.TimeUnit.SECONDS.toSeconds(5)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        onlineTextView = findViewById(R.id.onlineTextView)
        temperatureTextView = findViewById(R.id.temperatureTextView)
        heaterTextView = findViewById(R.id.heaterTextView)
        modeTextView = findViewById(R.id.modeTextView)
        setpointTextView = findViewById(R.id.setpointTextView)

        findViewById<Button>(R.id.modeButton).setOnClickListener {
            if (deviceOnline) {
                if (heaterOn) turnHeaterOff = true else turnHeaterOn = true
                invokeMethod()
            }
        }

        findViewById<Button>(R.id.heaterButton).setOnClickListener {
            if (deviceOnline) {
                if (isAutomatic) setManual = true else setAutomatic = true
                invokeMethod()
            }
        }

        findViewById<Button>(R.id.setpointUpButton).setOnClickListener {
            setpoint += 0.1f
            invokeMethod()
        }

        findViewById<Button>(R.id.setpointDownButton).setOnClickListener {
            setpoint -= 0.1f
            invokeMethod()
        }
    }

    private val exceptionRunnable: Runnable = Runnable {
        val builder = AlertDialog.Builder(applicationContext)
        builder.setMessage(lastException)
        builder.show()
        println(lastException)
    }

    private val methodResultRunnable: Runnable = Runnable {
        Toast.makeText(
                applicationContext,
                if (result != null)
                    "Status: ${result!!.status}, Payload: ${result!!.payload}"
                else
                    "Received empty response",
                Toast.LENGTH_LONG
        ).show()
    }

    private fun invokeMethod() {
        Thread(Runnable {
            result = null
            val payload: HashMap<String?, Any?> = object : HashMap<String?, Any?>() {
                init {
                    put("setpoint", setpoint.toString())
                    put("workMode", if(isAutomatic) "automatic" else "manual")
                    put("heater", if(heaterOn) "ON" else "OFF")
                }
            }
            try {
                methodClient = DeviceMethod.createFromConnectionString(connString)
                result = methodClient!!.invoke(deviceId, "setSendMessagesInterval", responseTimeout, connectTimeout, payload)
                if (result == null) {
                    throw IOException("Method invoke returns null")
                } else {
                    handler.post(methodResultRunnable)
                }
            } catch (e: Exception) {
                lastException = "Exception while trying to invoke direct method: $e"
                handler.post(exceptionRunnable)
            }
        }).start()
    }

}